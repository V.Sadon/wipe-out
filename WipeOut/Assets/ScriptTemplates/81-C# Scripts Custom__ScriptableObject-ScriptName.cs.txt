using UnityEngine;

    #ROOTNAMESPACEBEGIN#
[CreateAssetMenu(fileName = "#SCRIPTNAME# ", menuName = "App/ScriptableObjects/#SCRIPTNAME#")]
public class #SCRIPTNAME# : ScriptableObject 
{
	#NOTRIM#
} 
#ROOTNAMESPACEEND#